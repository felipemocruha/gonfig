package gonfig

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type SomeConfig struct {
	Test string `yaml:"test"`
}

type SomeNestedConfig struct {
	AnotherTest string     `yaml:"another_test"`
	SM          SomeConfig `yaml:"sm"`
}

func TestLoadConfigInvalidPath(t *testing.T) {
	path := "I really hope this path does not exist ;)"

	assert.IsType(t, &InvalidPathProvided{}, loadConfig(path, &SomeConfig{}))
}

func TestLoadConfigDecodeError(t *testing.T) {
	//setup
	content := `"damn!"::: "this is not valid YAML!!!!"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	assert.IsType(t, &DecodeError{}, loadConfig("config.yaml", &SomeConfig{}))

	//teardown
	os.Remove("config.yaml")
}

func TestLoadConfigFlat(t *testing.T) {
	//setup
	content := `test: "hello"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	config := &SomeConfig{}
	err := loadConfig("config.yaml", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeConfig{Test: "hello"}, *config)

	//teardown
	os.Remove("config.yaml")
}

func TestLoadConfigFlatWithUnknowFields(t *testing.T) {
	//setup
	content := `
test: "hello"
test2: 1
test4: false`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	config := &SomeConfig{}
	err := loadConfig("config.yaml", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeConfig{Test: "hello"}, *config)

	//teardown
	os.Remove("config.yaml")
}

func TestLoadConfigNested(t *testing.T) {
	//setup
	content := `
another_test: "hello"
sm:
  test: "hi"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	config := &SomeNestedConfig{}
	want := SomeNestedConfig{
		AnotherTest: "hello",
		SM:          SomeConfig{Test: "hi"},
	}

	err := loadConfig("config.yaml", config)
	assert.Nil(t, err)
	assert.Equal(t, want, *config)

	//teardown
	os.Remove("config.yaml")
}

func TestLoadFromPathFlat(t *testing.T) {
	//setup
	content := `test: "hello"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	config := &SomeConfig{}
	err := LoadFromPath("config.yaml", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeConfig{Test: "hello"}, *config)

	//teardown
	os.Remove("config.yaml")
}

func TestLoadFromEnvVarNotFound(t *testing.T) {
	//setup
	//assert.Nil(t, os.Setenv("CONFIG_PATH", "I really hope this path does not exist ;)"))

	config := &SomeConfig{}
	err := LoadFromEnv("CONFIG_PATH", config)

	assert.IsType(t, &ConfigNotFound{}, err)

	//teardown
	os.Unsetenv("CONFIG_PATH")
}

func TestLoadFromEnvVarFound(t *testing.T) {
	//setup
	content := `test: "hello"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))
	assert.Nil(t, os.Setenv("CONFIG_PATH", "config.yaml"))

	config := &SomeConfig{}
	err := LoadFromEnv("CONFIG_PATH", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeConfig{Test: "hello"}, *config)

	//teardown
	os.Unsetenv("CONFIG_PATH")
	os.Remove("config.yaml")
}

func TestLoadFromEnvWithFallback(t *testing.T) {
	//setup
	content := `test: "hello"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	config := &SomeConfig{}
	err := LoadFromEnv("CONFIG_PATH", config, "config.yaml")

	assert.Nil(t, err)
	assert.Equal(t, SomeConfig{Test: "hello"}, *config)

	//teardown
	os.Unsetenv("CONFIG_PATH")
	os.Remove("config.yaml")
}

func TestLoadFromEnvWithFallback2(t *testing.T) {
	//setup
	content := `test: "hello"`
	assert.Nil(t, ioutil.WriteFile("config.yaml", []byte(content), 0644))

	config := &SomeConfig{}
	err := LoadFromEnv("CONFIG_PATH", config, "config2.yaml", "config.yaml")

	assert.Nil(t, err)
	assert.Equal(t, SomeConfig{Test: "hello"}, *config)

	//teardown
	os.Unsetenv("CONFIG_PATH")
	os.Remove("config.yaml")
}
