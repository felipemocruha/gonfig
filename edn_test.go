package gonfig

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type SomeEdnConfig struct {
	Test string `edn:"test"`
}

type SomeNestedEdnConfig struct {
	AnotherTest string        `edn:"another_test"`
	SM          SomeEdnConfig `edn:"sm"`
}

func TestLoadConfigEdnInvalidPath(t *testing.T) {
	path := "I really hope this path does not exist ;)"

	assert.IsType(t, &InvalidPathProvided{}, loadConfigEdn(path, &SomeEdnConfig{}))
}

func TestLoadConfigEdnDecodeError(t *testing.T) {
	//setup
	content := `"damn!"::: "this is not valid EDN!!!!"`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	assert.IsType(t, &DecodeError{}, loadConfigEdn("config.edn", &SomeEdnConfig{}))

	//teardown
	os.Remove("config.edn")
}

func TestLoadConfigEdnFlat(t *testing.T) {
	//setup
	content := `{:test "hello"}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	config := &SomeEdnConfig{}
	err := loadConfigEdn("config.edn", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeEdnConfig{Test: "hello"}, *config)

	//teardown
	os.Remove("config.edn")
}

func TestLoadConfigEdnFlatWithUnknowFields(t *testing.T) {
	//setup
	content := `
{:test  "hello"
 :test2 1
 :test4 false}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	config := &SomeEdnConfig{}
	err := loadConfigEdn("config.edn", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeEdnConfig{Test: "hello"}, *config)

	//teardown
	os.Remove("config.edn")
}

func TestLoadConfigEdnNested(t *testing.T) {
	//setup
	content := `
{:another_test "hello"
 :sm {:test "hi"}}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	config := &SomeNestedEdnConfig{}
	want := SomeNestedEdnConfig{
		AnotherTest: "hello",
		SM:          SomeEdnConfig{Test: "hi"},
	}

	err := loadConfigEdn("config.edn", config)
	assert.Nil(t, err)
	assert.Equal(t, want, *config)

	//teardown
	os.Remove("config.edn")
}

func TestLoadEdnFromPathFlat(t *testing.T) {
	//setup
	content := `{:test "hello"}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	config := &SomeEdnConfig{}
	err := LoadEdnFromPath("config.edn", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeEdnConfig{Test: "hello"}, *config)

	//teardown
	os.Remove("config.edn")
}

func TestLoadEdnFromEnvVarNotFound(t *testing.T) {
	config := &SomeEdnConfig{}
	err := LoadEdnFromEnv("CONFIG_PATH", config)

	assert.IsType(t, &ConfigNotFound{}, err)

	//teardown
	os.Unsetenv("CONFIG_PATH")
}

func TestLoadEdnFromEnvVarFound(t *testing.T) {
	//setup
	content := `{:test "hello"}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))
	assert.Nil(t, os.Setenv("CONFIG_PATH", "config.edn"))

	config := &SomeEdnConfig{}
	err := LoadEdnFromEnv("CONFIG_PATH", config)

	assert.Nil(t, err)
	assert.Equal(t, SomeEdnConfig{Test: "hello"}, *config)

	//teardown
	os.Unsetenv("CONFIG_PATH")
	os.Remove("config.edn")
}

func TestLoadEdnFromEnvWithFallback(t *testing.T) {
	//setup
	content := `{:test "hello"}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	config := &SomeEdnConfig{}
	err := LoadEdnFromEnv("CONFIG_PATH", config, "config.edn")

	assert.Nil(t, err)
	assert.Equal(t, SomeEdnConfig{Test: "hello"}, *config)

	//teardown
	os.Unsetenv("CONFIG_PATH")
	os.Remove("config.edn")
}

func TestLoadEdnFromEnvWithFallback2(t *testing.T) {
	//setup
	content := `{:test "hello"}`
	assert.Nil(t, ioutil.WriteFile("config.edn", []byte(content), 0644))

	config := &SomeEdnConfig{}
	err := LoadEdnFromEnv("CONFIG_PATH", config, "config2.edn", "config.edn")

	assert.Nil(t, err)
	assert.Equal(t, SomeEdnConfig{Test: "hello"}, *config)

	//teardown
	os.Unsetenv("CONFIG_PATH")
	os.Remove("config.edn")
}
