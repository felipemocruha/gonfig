package gonfig

import (
	"io/ioutil"
	"os"

	"olympos.io/encoding/edn"
)

func loadConfigEdn(path string, config interface{}) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return newInvalidPathProvided(err)
	}

	if err = edn.Unmarshal(data, config); err != nil {
		return newDecodeError(err)
	}

	return nil
}

//LoadEdnFromPath takes a path, loads and decodes an EDN file into config struct with expected fields.
func LoadEdnFromPath(path string, config interface{}) error {
	return loadConfigEdn(path, config)
}

// LoadEdnFromEnv takes an environment variable, a config struct and optional paths where
// the config could be if env var value is empty.

// LoadEdnFromEnv in case of fallback options, returns the first one it can find.
// Case none of the fallback paths is valid, it'll return a 'ConfigNotFound' error.
func LoadEdnFromEnv(envVar string, config interface{}, fallback ...string) error {
	configPath := os.Getenv(envVar)

	var err error
	if configPath != "" {
		return loadConfigEdn(configPath, config)
	}

	if configPath == "" && len(fallback) != 0 {
		for _, f := range fallback {
			if err = loadConfigEdn(f, config); err == nil {
				return nil
			}
		}
	}

	return newConfigNotFound(err)
}
