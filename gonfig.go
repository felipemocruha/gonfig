package gonfig

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

type ConfigNotFound struct {
	message string
	cause   error
}

func (e *ConfigNotFound) Error() string {
	return fmt.Sprintf("%v: %v", e.message, e.cause)
}

func newConfigNotFound(cause error) *ConfigNotFound {
	return &ConfigNotFound{
		message: "failed to find config file on specified locations",
		cause:   cause,
	}
}

type InvalidPathProvided struct {
	message string
	cause   error
}

func (e *InvalidPathProvided) Error() string {
	return fmt.Sprintf("%v: %v", e.message, e.cause)
}

func newInvalidPathProvided(cause error) *InvalidPathProvided {
	return &InvalidPathProvided{
		message: "failed to read config from provided path",
		cause:   cause,
	}
}

type DecodeError struct {
	message string
	cause   error
}

func (e *DecodeError) Error() string {
	return fmt.Sprintf("%v: %v", e.message, e.cause)
}

func newDecodeError(cause error) *DecodeError {
	return &DecodeError{
		message: "failed to read config from provided path",
		cause:   cause,
	}
}

func loadConfig(path string, config interface{}) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return newInvalidPathProvided(err)
	}

	if err = yaml.Unmarshal(data, config); err != nil {
		return newDecodeError(err)
	}

	return nil
}

//LoadFromPath takes a path, loads and decodes a YAML into config struct with expected fields.
func LoadFromPath(path string, config interface{}) error {
	return loadConfig(path, config)
}

// LoadFromEnv takes an environment variable, a config struct and optional paths where
// the config could be if env var value is empty.

// LoadFromEnv in case of fallback options, returns the first one it can find.
// Case none of the fallback paths is valid, it'll return a 'ConfigNotFound' error.
func LoadFromEnv(envVar string, config interface{}, fallback ...string) error {
	configPath := os.Getenv(envVar)

	var err error
	if configPath != "" {
		return loadConfig(configPath, config)
	}

	if configPath == "" && len(fallback) != 0 {
		for _, f := range fallback {
			if err = loadConfig(f, config); err == nil {
				return nil
			}
		}
	}

	return newConfigNotFound(err)
}
