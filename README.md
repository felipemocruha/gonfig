# gonfig

Usage:

## YAML

Take an example config file:

```yaml
api-url: "https://my-fucking-awesome-api.mydomain.com"
credentials:
    user: "awesome-user"
    password: "1703dc79cce842638fba56fcb611c471"
```

Load config into your Go program:

```go
package main

import (
    "gitlab.com/felipemocruha/gonfig"
)

type credentials struct {
    User     string `yaml:"user"`
    Password string `yaml:"password"`
}

type Config struct {
    ApiURL      string      `yaml:"api-url"`
    Credentials credentials `yaml:"credentials"`
}

func main() {
    config := &Config{}

    // Loading from path
    if err := gonfig.LoadFromPath("config.yaml", config); err != nil {
        // Treat errors
    }
    
    // Loading path from Environment Variable
    if err := gonfig.LoadFromEnv("CONFIG_PATH", config); err != nil {
        // Treat errors
    }
    
    // Loading path from Environment Variable with fallback options
    if err := gonfig.LoadFromEnv("CONFIG_PATH", config, "/home/user/config.yaml"); err != nil {
        // Treat errors
    }
    
    ...
}
```


## EDN

Take an example config file:

```edn
{:api-url "https://my-fucking-awesome-api.mydomain.com"
 :credentials {:user     "awesome-user"
               :password "1703dc79cce842638fba56fcb611c471"}}
```

Load config into your Go program:

```go
package main

import (
    "gitlab.com/felipemocruha/gonfig"
)

type credentials struct {
    User     string `edn:"user"`
    Password string `edn:"password"`
}

type Config struct {
    ApiURL      string      `edn:"api-url"`
    Credentials credentials `edn:"credentials"`
}

func main() {
    config := &Config{}

    // Loading from path
    if err := gonfig.LoadEdnFromPath("config.edn", config); err != nil {
        // Treat errors
    }
    
    // Loading path from Environment Variable
    if err := gonfig.LoadEdnFromEnv("CONFIG_PATH", config); err != nil {
        // Treat errors
    }
    
    // Loading path from Environment Variable with fallback options
    if err := gonfig.LoadEdnFromEnv("CONFIG_PATH", config, "/home/user/config.edn"); err != nil {
        // Treat errors
    }
    
    ...
}
```


Now go on with your life ;)

Merge Requests are welcome!
