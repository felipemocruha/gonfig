module gitlab.com/felipemocruha/gonfig

go 1.14

require (
	github.com/stretchr/testify v1.3.0
	gopkg.in/yaml.v2 v2.2.2
	olympos.io/encoding/edn v0.0.0-20200308123125-93e3b8dd0e24
)
